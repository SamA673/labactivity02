package hellopackage; 
import java.util.Scanner;
import secondpackage.Utilities; 

// import java.util.Random; 

public class Greeter{
    public static void main (String[] args){
        // This is to resolve conflicts where classes have the same name.
        // Better to import...
        // java.util.Random rand = new java.util.Random();
        
    Scanner scan = new Scanner (System.in);
    System.out.println("Input integer");
    int num = scan.nextInt(); 
    num = Utilities.doubleMe(num);
    System.out.println(num); 

    scan.close(); 

    }



}